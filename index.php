<?
$h1         = 'Só Molas';
$title      = 'Início';
$desc       = 'Se procura por '.$h1.', você encontra nos resultados do Soluções Industriais, receba diversos orçamentos com mais de 100 empresas do Brasil ao mesmo te';
$var        = 'Home';
include('inc/head.php');
?>
</head>
<body>
<? include('inc/topo.php'); ?>
<!-- slider -->
<section class="cd-hero">
	<div class="title-main"><h1><?=$h1?></h1></div>
	<ul class="cd-hero-slider autoplay">
		<li class="selected">
			<div class="cd-full-width">
				<h2>Mola de aço inox</h2>
				<p>Utilizadas principalmente nas indústrias petroquímicas e petrolíferas, por ter seu grande benefício de resistência ao calor e a corrosão.</p>
				<a href="<?=$url?>mola-de-aco-inox" class="cd-btn">Saiba mais</a>
			</div>
		</li>
		<li>
			<div class="cd-full-width">
				<h2>Fábrica de molas em sp</h2>
				<p>Diversos tamanhos, espessuras e formatados, possibilitando ao comprador a escolha da peça que melhor se adéqua a sua necessidade.</p>
				<a href="<?=$url?>fabrica-de-molas-em-sp" class="cd-btn">Saiba mais</a>
			</div>
		</li>
		<li>
			<div class="cd-full-width">
				<h2>Tipos de molas</h2>
				<p>Molas são elementos que oferecem elasticidade, podendo sofrer modificações diante da aplicação da força sobre ele, armazenando-a para processos diversos.</p>
				<a href="<?=$url?>tipos-de-molas" class="cd-btn">Saiba mais</a>
			</div>
		</li>
	</ul>
	<div class="cd-slider-nav">
		<nav>
			<span class="cd-marker item-1"></span>
			<ul>
				<li class="selected"><a href="#"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
			</ul>
		</nav>
	</div>
</section>
<main>
	<section class="wrapper-main">
		<!-- quadros -->
		<div class="main-center">
			<div class=" quadro-2 ">
				<h2>Molas</h2>
				<div class="div-img">
					<p>As molas de aço, cobre, inox,entre outros estão presentes no seu dia a dia, por meio de objetos como: fechaduras, brinquedos, coqueteleiras, máquinas, instrumentos musicais, anzóis de pesca, e eletrodomésticos.</p>
				</div>
				<div class="gerador-svg">
					<img src="imagens/img-home/molas.jpg" alt="Molas" title="Molas">
				</div>
			</div>
			<div class=" incomplete-box">
				<ul>
					<li>Existem diversos tipos de molas, que podem ser usadas de acordo com sua necessidade. Entre as molas disponíveis no mercado, estão as <b>molas para torção</b>, <b>molas para compressão</b>, e <b>molas para tração</b>.</li>
					<li>Outros tipos de molas:</li>
					<li><i class="fas fa-angle-right"></i> Molas para Retentor</li>
					<li><i class="fas fa-angle-right"></i> Molas de Fita / Espiral</li>
					<li><i class="fas fa-angle-right"></i> Estamparia</li>
					<li><i class="fas fa-angle-right"></i> Grampos</li>
					<li><i class="fas fa-angle-right"></i> Anéis</li>
				</ul>
				<a href="<?=$url?>onde-comprar-molas" class="btn-4">Saiba mais</a>
			</div>
		</div>
		<!-- icons -->
		<div id="content-icons">
			<div class="co-icon">
				<div class="quadro-icons animate" data-anime="in">
					<i class="fas fa-dollar-sign fa-7x"></i>
					<div>
						<p>Compare preços</p>
					</div>
				</div>
			</div>
			<div class="co-icon">
				<div class="quadro-icons animate" data-anime="in">
					<i class="fas fa-clock fa-7x"></i>
					<div>
						<p>Economize tempo</p>
					</div>
				</div>
			</div>
			<div class="co-icon">
				<div class="quadro-icons animate" data-anime="in">
					<i class="fas fa-handshake fa-7x"></i>
					<div>
						<p>Faça o melhor negócio</p>
					</div>
				</div>
			</div>
		</div>
		<!-- banners -->
		<section class="wrapper-img">
			<div class="txtcenter">
				<h2>Produtos &nbsp;<b>Relacionados</b></h2>
			</div>
			<div class="content-icons">
				<div class="produtos-relacionados-1">
					<figure>
						<a href="<?=$url?>molas-de-chapa">
							<div class="fig-img">
								<h2>Molas de Chapa</h2>
								<div class="btn-5"> Saiba Mais </div>
							</div>
						</a>
					</figure>
				</div>
				<div class="produtos-relacionados-2">
					<figure class="figure2">
						<a href="<?=$url?>mola-voluta">
							<div class="fig-img2">
								<h2>Mola Voluta</h2>
								<div class="btn-5"> Saiba Mais </div>
							</div>
						</a>
					</figure>
				</div>
				<div class="produtos-relacionados-3">
					<figure>
						<a href="<?=$url?>sistema-massa-mola">
							<div class="fig-img">
								<h2>Sistema Massa-Mola</h2>
								<div class="btn-5"> Saiba Mais </div>
							</div>
						</a>
					</figure>
				</div>
			</div>
		</section>
		<!-- galeria -->
		<section class="wrapper-destaque">
			<div class="destaque txtcenter">
				<h2>Galeria de &nbsp;<b>Produtos</b></h2>
				<div class="center-block txtcenter">
					<ul class="gallery">
						<li>
							<a href="<?=$url?>imagens/img-home/galeria/molas-1.jpg" class="lightbox" title="Molas">
								<img src="<?=$url?>imagens/img-home/galeria/thumbs/molas-1.jpg" title="Molas" alt="Molas">
							</a>
						</li>
						<li>
							<a href="<?=$url?>imagens/img-home/galeria/molas-2.jpg" class="lightbox" title="Molas de Tração">
								<img src="<?=$url?>imagens/img-home/galeria/thumbs/molas-2.jpg" alt="Molas de Tração" title="Molas de Tração">
							</a>
						</li>
						<li>
							<a href="<?=$url?>imagens/img-home/galeria/molas-3.jpg" class="lightbox"  title="Molas de Tração">
								<img src="<?=$url?>imagens/img-home/galeria/thumbs/molas-3.jpg" alt="Molas de Tração" title="Molas de Tração">
							</a>
						</li>
						<li>
							<a href="<?=$url?>imagens/img-home/galeria/molas-4.jpg" class="lightbox" title="Molas de Torção">
								<img src="<?=$url?>imagens/img-home/galeria/thumbs/molas-4.jpg" alt="Molas de Torção" title="Molas de Torção">
							</a>
						</li>
						<li>
							<a href="<?=$url?>imagens/img-home/galeria/molas-5.jpg" class="lightbox" title="Molas de Chapa">
								<img src="<?=$url?>imagens/img-home/galeria/thumbs/molas-5.jpg" alt="Molas de Chapa"  title="Molas de Chapa">
							</a>
						</li>
						<li>
							<a href="<?=$url?>imagens/img-home/galeria/molas-6.jpg" class="lightbox" title="Molas de Tração">
								<img src="<?=$url?>imagens/img-home/galeria/thumbs/molas-6.jpg" alt="Molas de Tração" title="Molas de Tração">
							</a>
						</li>
						<li>
							<a href="<?=$url?>imagens/img-home/galeria/molas-7.jpg" class="lightbox" title="Molas">
								<img src="<?=$url?>imagens/img-home/galeria/thumbs/molas-7.jpg" alt="Molas" title="Molas">
							</a>
						</li>
						<li>
							<a href="<?=$url?>imagens/img-home/galeria/molas-8.jpg" class="lightbox" title="Molas de Tração">
								<img src="<?=$url?>imagens/img-home/galeria/thumbs/molas-8.jpg" alt="Molas de Tração" title="Molas de Tração">
							</a>
						</li>
						<li>
							<a href="<?=$url?>imagens/img-home/galeria/molas-9.jpg" class="lightbox" title="Molas">
								<img src="<?=$url?>imagens/img-home/galeria/thumbs/molas-9.jpg" alt="Molas" title="Molas">
							</a>
						</li>
						<li>
							<a href="<?=$url?>imagens/img-home/galeria/molas-10.jpg" class="lightbox" title="Molas">
								<img src="<?=$url?>imagens/img-home/galeria/thumbs/molas-10.jpg" alt="Molas" title="Molas">
							</a>
						</li>
					</ul>
				</div>
			</div>
		</section>
	</section>
</main>
<? include('inc/footer.php'); ?>
<script src="<?=$url?>hero/js/modernizr.js"></script>
<script src="<?=$url?>hero/js/main.js"></script>

<script src="slick/slick.min.js"></script>
<script>
	$('.products').slick({
	  dots: true,
	  infinite: true,
	  speed: 300,
	  autoplay: true,
	  slidesToShow: 4,
	  slidesToScroll: 4,
	  responsive: [{
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});
</script>
</body>
</html>
