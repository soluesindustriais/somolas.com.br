<?
$h1 = "Produtos";
$title = "Produtos";
$desc = "Produtos sobre os produtos e serviços comercializados pela empresa. Clique aqui para saber mais detalhes. Dúvidas, entre em contato conosco agora mesmo";
$var = "Produtos";
include('inc/head.php');
?>
</head>

<body>
	<? include('inc/topo.php'); ?>
	<div class="wrapper">
		<main>
			<div class="content">
				<div class="breadcrumb">
					<div class="wrapper">
						<div class="bread__row">
							<nav aria-label="breadcrumb">
								<ol id="breadcrumb" class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
									<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
										<a href="' . $url . '" itemprop="item" title="Home">
											<span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Home » </span>
										</a>
										<meta itemprop="position" content="1" />
									</li>
									<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
										<span itemprop="name"> <?=$h1?> </span>
										<meta itemprop="position" content="2" />
									</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
				<h1>Produtos</h1>
				<article class="full">
					<p>Encontre diversos fornecedores de molas, cote agora mesmo!</p>
					<ul class="thumbnails-main">
						<li>
							<a rel="nofollow" href="<?= $url ?>compressao-categoria" title="Categoria - Compressão">
								<img src="<? $url ?>imagens/compressao.jpg" alt="Compressão" title="Compressão" />
							</a>
							<h2>
								<a href="<?= $url ?>compressao-categoria" title="Categoria - Compressão">
									Compressão
								</a>
							</h2>
						</li>
						<li>
							<a rel="nofollow" href="<?= $url ?>molas-categoria" title="Categoria - Molas">
								<img src="<? $url ?>imagens/molas.jpg" alt="Molas" title="Molas" />
							</a>
							<h2>
								<a href="<?= $url ?>molas-categoria" title="Categoria - Molas">
									Molas
								</a>
							</h2>
						</li>

						<!-- Inserção de Palavras 06-08-2020 -->

						<li>
							<a rel="nofollow" href="<?= $url ?>molas-de-compressao-categoria" title="Categoria - Molas de Compressão">
								<img src="<? $url ?>imagens/molas-de-compressao/molas-de-compressao-01.jpg" alt="Molas de compressão" title="Molas de compressão" />
							</a>
							<h2>
								<a href="<?= $url ?>molas-de-compressao-categoria" title="Categoria - Molas de Compressão">
									Molas de Compressão
								</a>
							</h2>
						</li>

						<li>
							<a rel="nofollow" href="<?= $url ?>artefato-e-pecas-de-arame-categoria" title="Categoria - Artefato e Peças de Arame">
								<img src="<? $url ?>imagens/artefato-e-pecas-de-arame/artefato-e-pecas-de-arame-01.jpg" alt="Artefato e Peças de Arame" title="Artefato e Peças de Arame" />
							</a>
							<h2>
								<a href="<?= $url ?>artefato-e-pecas-de-arame-categoria" title="Categoria - Artefato e Peças de Arame">
									Artefato e Peças de Arame
								</a>
							</h2>
						</li>

					</ul>
				</article>

			</div>
		</main>
	</div>
	<? include('inc/footer.php'); ?>
</body>

</html>