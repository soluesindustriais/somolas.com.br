// Define as constantes
const wppBody = $(".wppPopup__body"),
  wppPopup = $(".wppPopup");

// Configura o evento de tecla de escape
$(document).on("keyup", function (e) {
  if (e.key == "Escape") {
    Clear();
    wppPopup.removeClass("wppActive");
  }
});

// Configura o evento de clique no botão "X"
$(".wppClose").click(function () {
  Clear();
  wppPopup.removeClass("wppActive");
});

// Reset das configurações iniciais
const Clear = () => {
  $("[data-form]").each(function () {
    this.reset();
  });
  wppBody.show();
};

const Init = () => {
  // Define as variáveis
  let wppButtons = new Array(),
    wppButton = "",
    fixedPosition = 90;

  // Obtém os botões
  $("[data-button]").each(function () {
    wppButtons.push($(this).data("button"));
  });

  // Seta a posição do primeiro botão
  wppButton = $("[data-button = 0]").css("bottom", `${fixedPosition}px`);

  // Itera sobre os botões
  for (let i = 0; i <= wppButtons.length; i++) {
    if (i > 0) {
      // Incrementa a posição para o botão atual
      fixedPosition += 72;
    }

    // Obtém o botão da posição atual
    wppButton = $(`[data-button = ${i}]`);

    // Seta a posição do botão atual
    $(wppButton).css("bottom", `${fixedPosition}px`);

    // Configura o evento de clique no botão atual
    $(wppButton).click(function () {
      // Adiciona a classe ativa ao popup
      if (wppPopup.hasClass("wppActive")) {
        wppPopup.filter(".wppActive").removeClass("wppActive");
        Clear();
        $(this).next(wppPopup).addClass("wppActive");
      } else {
        $(this).next(wppPopup).addClass("wppActive");
      }
    });
  }

  $("[data-button = whatsappForm]").click(function () {
    wppPopup.filter(".wppActive").removeClass("wppActive");
    Clear();
    wppPopup.addClass("wppActive");
  });
};
Init();
