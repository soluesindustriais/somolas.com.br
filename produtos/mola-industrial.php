
    <?php
$h1 = "Mola industrial";
$title  =  $h1;
$cliente_minisite = "Walb Molas";
$minisite = "walbmolas";
$desc = "A mola industrial é projetada para atender às demandas de equipamentos industriais, garantindo resistência e eficiência. Solicite já uma cotação!";
include "inc/$minisite/inc/head.php";
?>
</head>

<body>
    <?php include "$prefix_includes" . "inc/formulario-personalizado.php" ?>
    <?php include "$prefix_includes" . "inc/topo.php"; ?>
    <?php include "$prefix_includes" . "inc/auto-breadcrumb.php" ?>

    <main class="mpi-page wrapper">
        <section class="product-container">
            <section class="product-information">
                <?php include "$prefix_includes" . "inc/product-images.php" ?>
                <?php include "$prefix_includes" . "inc/product-conteudo.php" ?>
            </section>
                <?php include "$prefix_includes" . "inc/product-aside.php" ?>
        </section>
        
        <?php include "$prefix_includes" . "inc/product-populares.php" ?>

    </main>
    <?php include "$prefix_includes" . "inc/footer.php"; ?>
</body>

</html>
    