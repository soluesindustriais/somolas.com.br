<section class="wrapper produtos-populares">
    <h2>Produtos Populares</h2>
    <div class="produtos-populares-container-cards">
        <?php
        foreach ($menuItems as $value_product_popular => $key_product_popular) {
            if (isset($key_product_popular["submenu"])) {
                foreach ($key_product_popular["submenu"] as $value_product_popular_2 => $key_product_popular_2) {
                    $create_product_popular[] = $value_product_popular_2;
                }
            }
        }
        shuffle($create_product_popular);
            $limit = 4;
            for ($i = 0; $i < $limit; $i++) {
                $code = $i+1;
            if(isset($create_product_popular[$i])){
                foreach ($menuItems as $value_products => $key_products) {
                    if (isset($key_products["submenu"])) {
                        $pathImgProductPop = $key_product_popular["submenu"][$create_product_popular[$i]]["url"];

                            if(file_exists($prefix_includes."imagens/informacoes/".$pathImgProductPop."-1.webp")){
                                $pathPopularProduct = $prefix_includes."imagens/informacoes/".$pathImgProductPop . "-1.webp";
                            } else {
                                $pathPopularProduct = $prefix_includes."imagens/informacoes/"."solucs.png";
                            }
                            
                        }
                }
                echo 
                '
                <a href="'.$link_minisite.$key_product_popular["submenu"][$create_product_popular[$i]]["url"].'" title="Obetenha mais inforamções sobre:'.$create_product_popular[$i].'">
                <div class="produtos-populares-card">
                <div class="produtos-populares-cod">
                <p>Cod: <b>#00'.$code   .'</b></p>
                <p>Em alta <i class="fa-solid fa-fire"></i></p>
                </div>
                <img src="'.$pathPopularProduct.'" alt="'.$create_product_popular[$i].'">
                <p>'.$create_product_popular[$i].'</p>
                </div>
                </a>
                ';
            }
        }
        ?>
    </div>
</section>