<?php session_start(); ?>

<!DOCTYPE html>

<html class="no-js" lang="pt-br">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php 
	$archives_subdomain = ["index.php", "sobre-nos.php","contato.php","pesquisa.php","mapa-site.php", "pesquisa.php"];
	$prefix_includes = "inc/$minisite/"; 
	
	if(in_array($archive, $archives_subdomain)){
		$prefix_includes = "produtos/inc/$minisite/"; 	
	} ;
	?>

	<?php 
		include "$prefix_includes"."inc/geral.php";
	?>
	<title><?php echo "$title - $nomeSite" ?></title>

	<?php 
	include "$prefix_includes"."inc/jquery.php";
	?>

	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	
	<script src="<?php echo $prefix_includes ?>js/lazysizes.min.js" defer></script>
	
	<link href="https://fonts.googleapis.com/css2?family=Bad+Script&family=Merriweather+Sans:ital,wght@0,300..800;1,300..800&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Sixtyfour&display=swap" rel="stylesheet">

	<!-- <link rel="stylesheet" href="css/style.css"> -->
	<style>
		<?php
		include "$prefix_includes"."css/style.css";
		include "$prefix_includes"."css/normalize.css";

		if ($isMobile) {
			include "$prefix_includes"."css/menu-hamburger.css";
		};

		include "$prefix_includes"."css/slick.css"
		?>
	</style>
	<style>
    .cards-informativos{
        display: flex;
        justify-content: space-around;
        margin: 0px auto;
        max-width: 1024px;
        align-items: center;
        flex-wrap: wrap;
    }
    .cards-informativos > 
    .cards-informativos-infos{
        background-color: #fff;
        box-shadow: 0px 0px 8px #31313131;
        padding: 16px;
        border-radius: 1rem;
        width: 300px;
        margin-top: 10px;
        margin: 20px 0px;
        height: 300px;
        cursor: pointer;
        transition: 0.3s ;
    }
    .cards-informativos-infos p{
        text-align: left;
    }
    .cards-informativos-infos i{
        color: var(--color-secundary);
        font-size: 1.8rem;
    }
    .cards-informativos-infos h3{
        text-align: center;
    }
    .cards-informativos-infos:hover{
        margin-top: 0px;
        transition: 0.3s ;
    }
    .card-informativo h2, .card-informativo span p{
        text-align: center;
        font-weight: bold;
    }
    .card-informativo span p{
        font-size: 1.1rem;
        margin: 0px;
    }
    .card-informativo h2{
        margin: 0px;
        font-size: 2rem;
        color: var(--color-secundary);
    }
</style>
	<script>
		<?php

		if ($isMobile) {
			include "$prefix_includes"."js/menu-hamburger.js";
		}
		?>
		<?php
		function remove_acentos($string)
		{
			return preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ç)/", "/(Ç)/"), explode(" ", "a A e E i I o O u U c C"), $string);
		}
		?>
	</script>
	<?php include "$prefix_includes"."inc/fancy.php"; ?>
	<base href="<?= $url ?>">
	<?php
	$desc = strip_tags($desc);
	$desc = str_replace('  ', ' ', $desc);
	$desc = str_replace(' ,', ',', $desc);
	$desc = str_replace(' .', '.', $desc);
	if (mb_strlen($desc, "UTF-8") > 160) {
		$desc = mb_substr($desc, 0, 159);
		$finalSpace = strrpos($desc, " ");
		$desc = substr($desc, 0, $finalSpace);
		$desc .= ".";
	} else if (mb_strlen($desc, "UTF-8") < 140 && mb_strlen($desc, "UTF-8") > 130) {
		$desc .= "... Saiba mais.";
	}
	?>
	<meta name="description" content="<?= ucfirst($desc) ?>">
	<meta name="keywords" content="<?= $h1 . ', ' . $nomeSite ?>">
	<meta name="geo.position" content="<?= $latitude . ";" . $longitude ?>">
	<meta name="geo.placename" content="<?= $cidade . "-" . $UF ?>">
	<meta name="geo.region" content="<?= $UF ?>-BR">
	<meta name="ICBM" content="<?= $latitude . ";" . $longitude ?>">
	<meta name="robots" content="index,follow">
	<meta name="rating" content="General">
	<meta name="revisit-after" content="7 days">
	<link rel="canonical" href="<?= $url . $urlPagina ?>">

	<link rel="shortcut icon" href="<?= $prefix_includes ?>imagens/logo-solucs.png">

	<meta property="og:region" content="Brasil">
	<meta property="og:title" content="<?= $title . " - " . $nomeSite ?>">
	<meta property="og:type" content="article">
	<?php
	if (file_exists($url . $pastaSocialMedia . $urlPagina . "-01.jpg")) {
	?>
		<meta property="og:image" content="<?= $url . $pastaSocialMedia . $urlPagina ?>-01.jpg">
	<?php
	}
	?>
	<meta property="og:url" content="<?= $url . $urlPagina ?>">
	<meta property="og:description" content="<?= $desc ?>">
	<meta property="og:site_name" content="<?= $nomeSite ?>">
	<!-- Desenvolvido por <?= $creditos . " - " . $siteCreditos ?> -->
	
	<link rel="preload" as="style" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" onload="this.rel='stylesheet'">