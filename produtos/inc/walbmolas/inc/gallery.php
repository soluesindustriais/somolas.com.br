<ul class="mpi-gallery">
  <?php
  $imagens = glob("imagens/thumbs/" . $urlPagina . "-{,[0-9]}[0-9].png", GLOB_BRACE);
  
  if (empty($imagens)) {
    // Se não encontrar nenhuma imagem, exibe uma imagem aleatória do caminho especificado
    $randomImageNumber = rand(1, 4); // Gera um número aleatório entre 1 e 4.
    $randomImagePath = "imagens/$categ/$categ-$randomImageNumber.webp";
    echo '<li>
            <a href="' . $url . $randomImagePath . '" data-fancybox="group1" class="lightbox" title="' . $h1 . '" data-caption="' . $h1 . '">
              <img src="' . $url . $randomImagePath . '" alt="' . $h1 . '" title="' . $h1 . '">
            </a>
          </li>';
  } else {
    // Se encontrar imagens, exibe cada uma
    foreach ($imagens as $key => $imagem) {
      echo '<li>
              <a href="' . $url . $imagem . '" data-fancybox="group1" class="lightbox" title="' . $h1 . '" data-caption="' . $h1 . '">
                <img src="' . $url . $imagem . '" alt="' . $h1 . '" title="' . $h1 . '">
              </a>
            </li>';
    }
  }
  ?>
</ul>
