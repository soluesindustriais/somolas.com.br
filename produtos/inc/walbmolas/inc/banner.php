
	<?php if (!$isMobile) : ?>
		<div class="slick-banner">
			<div style="--background: linear-gradient(rgba(36, 36, 36, 0.7) 50%, rgba(36, 36, 36, 0.7) 50%), url('<?= $url ?>imagens/banner/banner-1.webp')">
				<div>
					<div class="content-banner justify-content-evenly row">
						<div>
							<h1>Walb Molas</h1>
							<p>Molas & Artefatos de Arame</p>
							<a class="btn" href="<?= $url ?>catalogo" title="Página de produtos">Clique</a>
						</div>
						<div>
							<img src="<?= $url ?>imagens/clientes/logo-categoria.jpg" alt="" title="" class="slick-thumb">
						</div>
					</div>
				</div>
			</div>
			<div style="--background: linear-gradient(rgba(36, 36, 36, 0.5) 50%, rgba(36, 36, 36, 0.5) 50%), url('<?= $url ?>imagens/banner/banner-2.webp')">
				<div class="content-banner">
					<h2>Walb Molas</h2>
					<p>Tecnologia que Faz a Diferença.</p>
					<a class="btn" href="<?= $url ?>sobre-nos" title="Página sobre nós">Clique</a>
				</div>
			</div>
	
		</div>
	<?php endif; ?>