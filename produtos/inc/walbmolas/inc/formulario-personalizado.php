<div class="modal-orcamento-close" id="modal-orcamento">
    <div class="modal-orcamento modal-orcamento-background" onclick="closemodalorcamento()"></div>
    <div class="modal-orcamento-form">
        <div class="modal-orcamento-form-left">
            <h2>Solicite o seu orçamento</h2>
            <?php include "$prefix_includes" . "imagens/modal-svg-2.svg" ?>
            <div class="modal-orcamento-form-contact">
                <p><i class="fa-solid fa-phone"></i> Telefone:<br> <?php echo $cliente_telefone ?></p>
                <p><i class="fa-solid fa-envelope"></i> E-mail:<br> <?php echo $cliente_email ?></p>
            </div>
        </div>
        <div class="modal-orcamento-form-right">
            <span class="close-modal-orcamento" id="close-modal-orcamento" onclick="closemodalorcamento()"><i class="fa-solid fa-x"></i></span>
            <h3><span class="secundary-color-orcamento">Produto: </span><?php echo $h1 ?></h3>
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
            <div data-sdk-form-builder-solucoes data-form-id="<?php echo  $idForm?>"></div>
            <script src="https://painel.solucoesindustriais.com.br/js/sdk/formBuilderSdk.js"></script>
            <script>
                $(document).ready(function() {
                    // Inicialize o FormRendererSDK
                    FormRendererSDK.initialize();
                    // Registre callbacks para eventos, se necessário
                    FormRendererSDK.on('formRendered', function(container) {
                        // Lógica após a renderização bem-sucedida do formulário
                        $("input[type='tel']").mask("(00) 00000-0000");
                    });
                });
            </script>
        </div>
    </div>
</div>

<script>
    function solicitarocarmento() {
        const modal_orcamento = document.getElementById("modal-orcamento");
        modal_orcamento.classList = "modal-orcamento";

    }

    function closemodalorcamento() {
        const modal_orcamento = document.getElementById("modal-orcamento");
        modal_orcamento.classList = "modal-orcamento-close";
    }

    document.onkeydown = function(e) {
        if (e.key === 'Escape') {
            closemodalorcamento()
        }
    }
</script>