
<section class="wrapper card-informativo">
    <span><p>O que nós fornecemos</p></span>
    <h2>Nossos principais focos</h2>
        <div class="cards-informativos">
        <div class="cards-informativos-infos">
            <i class="fa-solid fa-map"></i>
            <h3>Missão</h3>
            <p>Produzir molas e artefatos de arames em geral, oferecendo serviços de qualidade e valor agregado, a preços competitivos, visando atender as necessidades e superar expectativas de nossos clientes.</p>
        </div>
        <div class="cards-informativos-infos">
            <i class="fa-solid fa-eye"></i>
            <h3>Visão</h3>
        <p>Ser líder em nosso segmento por meio de inovação, modernização e excelência na gestão, oferecendo qualidade diferenciada e conquistando credibilidade e respeito no mercado.</p>
    </div>
    <div class="cards-informativos-infos">
        <i class="fa-solid fa-handshake"></i>
        <h3>Valores</h3>
        <p>Compromisso com a excelência, relações de confiança, desenvolvimento humano, progresso social e impacto positivo na sociedade. </p>
    </div>
</div>
</section>
