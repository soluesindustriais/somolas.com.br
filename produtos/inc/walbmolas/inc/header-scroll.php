<script>
var altura = document.getElementById('scrollheader');
if (altura !== null) {
  altura = altura.offsetHeight;
  if (window.innerWidth > 765) {
    window.onscroll = function() {
      Scroll();
    };
  }
  function Scroll() {
    if (document.body.scrollTop > 30 || document.documentElement.scrollTop > 30) {
      document.getElementById("scrollheader").classList.add("topofixo");
      document.getElementById("header-block").style.display = "block";
      document.getElementById("header-block").style.height = altura + "px";
    } else {
      document.getElementById("scrollheader").classList.remove("topofixo");
      document.getElementById("header-block").style.display = "none";
    }
  }
}
</script>