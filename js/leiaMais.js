function toggleReadMore() {
    var content = document.querySelector('.article-content');
    var readMoreButton = document.querySelector('.read-more-button');
    var closeButton = document.querySelector('.close-button');

    if (content.style.maxHeight) {
        content.style.maxHeight = null;
        closeButton.style.display = 'none';
        readMoreButton.style.display = 'block';

    } else {
        content.style.maxHeight = "none";
        closeButton.style.display = 'block';
        readMoreButton.style.display = 'none';

        // Rola para baixo quando o botão "read-more-button" é clicado
        content.scrollIntoView({
            top: 1000,
            behavior: 'smooth',
            block: 'start'
        });
    }
}


function closeAndScroll() {
    // Chama a função toggleReadMore para fechar a div
    toggleReadMore();

    // Rola para cima quando o botão "close-button" é clicado
    window.scrollTo({
        top: 0,
        behavior: 'smooth'
    });
}