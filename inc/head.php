<!DOCTYPE html>
<html class="no-js" lang="pt-br">
<head>
	<meta charset="utf-8">
	<?php include('inc/geral.php'); ?>
	<title><?= mb_strlen($title." - ".$nomeSite, "UTF-8") > 65 ? $title : $title." - ".$nomeSite; ?></title>
	<link rel="shortcut icon" href="<?=$url?>imagens/img-home/favicon.png">
	<base href="<?=$url?>">
	<meta name="description" content="<?= mb_strlen($desc, 'UTF-8') < 160 ? ucfirst($desc) : mb_substr($desc, 0, 155, 'UTF-8').'...'; ?>">
	<meta name="keywords" content="<?=$h1.", ".$key?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="geo.position" content="<?=$latitude.";".$longitude?>">
	<meta name="geo.placename" content="<?=$cidade."-".$UF?>">
	<meta name="geo.region" content="<?=$UF?>-BR">
	<meta name="ICBM" content="<?=$latitude.";".$longitude?>">
	<meta name="robots" content="index,follow">
	<meta name="rating" content="General">
	<meta name="revisit-after" content="7 days">
	<link rel="canonical" href="<?=$url.$urlPagina?>">
	<?php
		if ( $author == ''){ echo '<meta name="author" content="'.$nomeSite.'">'; }
		else{ echo '<link rel="author" href="'.$author.'">'; }
	?>

	<meta property="og:region" content="Brasil">
	<meta property="og:title" content="<?=$title." - ".$nomeSite?>">
	<meta property="og:type" content="article">
  <?php if (file_exists($url.$pasta.$urlPagina."-01.jpg")) { ?>
    <meta property="og:image" content="<?=$url.$pasta.$urlPagina?>-01.jpg">
  <?php } ?>
	<meta property="og:url" content="<?=$url.$urlPagina?>">
	<meta property="og:description" content="<?=$desc?>">
	<meta property="og:site_name" content="<?=$nomeSite?>">
	<meta property="fb:admins" content="<?=$idFacebook?>">

	<style>
		<?php include ("css/normalize.css"); ?>
	</style>
	<link rel="stylesheet" type="text/css" href="slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
	<link rel="stylesheet" type="text/css" href="css/fontawesome.css">
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>

	<script src="<?=$url?>js/lazysizes.min.js" async></script>
	<script src="<?=$url?>js/leiaMais.js" async></script>
