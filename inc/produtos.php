<?php
  include 'vetProduto.php';
  foreach ($produtos as $key => $produto) {
    $produto['desc'] = mb_strlen($produto['desc'], 'UTF-8') < 85 ? $produto['desc'] : mb_substr($produto['desc'], 0, 85, 'UTF-8').'...';
    $item  = "<div class='product-detail'>";
    $item .= "<img src='". $url ."imagens/portal/thumbs/".$produto['img']."' alt='".$produto['title']."' title='".$produto['title']."'>";
    $item .= "<h4>".$produto['title']."</h4>";
    $item .= "<p>".$produto['desc']."</p>";
    $item .= "<a class='btn-ver' href='".$produto['link']."'>Ver mais</a>";
    $item .= "</div>";
    echo $item;
  }
?>
