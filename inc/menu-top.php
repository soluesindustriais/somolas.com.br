<div class="logo-top">
  <a href="<?=$url?>" title="Início">
  <img src="imagens/img-home/logo.png" alt="Logo - Loca Andaimes" title="Logo - Loca Andaimes"></a>
</div>
<ul>
	<li><a href="<?=$url?>" title="Página inicial"><i class="fas fa-home"></i> Início</a></li>
	<li class="dropdown"><a href="<?=$url?>produtos" title="Produtos"><i class="fas fa-tags"></i> Produtos</a>
		<ul class="sub-menu">
		  <? include('inc/sub-menu.php');?>
		</ul>
	</li>
	<li><a href="<?=$url?>sobre-nos"><i class="fas fa-user"></i> Sobre Nós</a></li>
	<li><a href="<?=$url?>blog"><i class="fas fa-book"></i> Blog</a></li>
	<li><a class="botao-anuncio" id="btn-solucs-topo" target="blanck" href="https://faca-parte.solucoesindustriais.com.br/" title="Gostaria de anunciar?">Gostaria de Anunciar?</a></li>
</ul>
